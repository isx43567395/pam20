# PAM

## @Alejandro Lopez ASIX M06 2020-2021


Podeu trobar les imatges docker al Dockehub de [zeuslawl](https://hub.docker.com/u/zeuslawl/)

### Imatges:

* **zeuslawl/pam20:ldap** host pam per practicar *PAM* amb autenticació local
  unix (*pam_unix.so*) i autenticació LDAP (amb *pam_ldap.so*). Utilitza el paquet
  *nss-pam-ldapd*. Cal configurar: *ldap.conf*, *nslcd*, *nscd* i *nssitch*.
  L'autentitació es configura al *system-auth*.


  Atenció, cal usar en el container --privileged per poder fer els muntatges nfs.

```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d zeuslawl/ldap20:group 
$ docker run --rm --name pam.edt.org -h pam.edt.org --net 2hisix --privileged -it zeuslawl/pam20:ldap
