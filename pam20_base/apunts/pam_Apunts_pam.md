# Apunts PAM

cd /etc/pam.d/ --> Directori de fitxers de programes PAM Aware
```
[root@pam pam.d]# cat chfn
#%PAM-1.0
auth       sufficient   pam_rootok.so
auth       include      system-auth
account    include      system-auth
password   include      system-auth
session    include      system-auth
```

Els diàlegs entre PAM i els clients es realitzen a través de la API.

Application PAM aware --> Aplicació que sap que ha de fer servir PAM.

ls /usr/lib64/security/ --> Conté els mòduls instal·lats de PAM

.so --> shared object

ldd --> List Dinamic Dependencies (Shared Object)

Llibreries que necessita per funciona chfn --> `ldd /usr/bin/chfn`

Qualsevol programa PAM Aware sense fitxer específic de configuració se li aplica el fitxer `/etc/pam.d/other`

Afegir al final de /etc/login.defs per a fer proves a chfn amb usuaris no root.
CHFN_RESTRICT no

# Grups de GESTIÓ

## Account

Serveis de verificació de comptes d'usuari.

Serveix per a permetre o denegar l'accès segons la hora, comprovar si el compte ha caducat. (pam_time.so)

Serveix per a validar que el compte no estigui bloquejat, caducat. (pam_unix.so)

Objectes que hi treballen:
+ pam_time.so només treballa al grup de gestió.
+ pam_unix.so
+ pam_ldap.so
+ pam_localuser.so

## Auth

Comprova la identitat dels usuaris, ofereix un sistema de credencials que permeten a l'usuari guanyar certs permisos (Definits per l'admin)

Authentication, autenticació, sóc o no sóc el Pau, demostra qui sóc.

(No confondre amb: autz --> Authorization, autorització, a qué tinc dret)

Objectes que hi treballen:
+ pam_env.so
+ pam_unix.so
+ pam_ldap.so
+ pam_mount.so

## Password

S'encarrega de mantenir actualitzat l'element d'autenticació associat a cada usuari, Ex: contrasenya, empremtes dactilars, escàner de retina.

Es crida en executar la ordre passwd

Objectes que hi treballen:
+ pam_pwquality.so
+ pam_unix.so
+ pam_ldap.so
+ pam_mount.so

## Session

Administra les sessions dels usuaris. Realitza accions un cop ja autenticats els usuaris, per exemple crear el seu home o muntar directoris.

Objectes que hi treballen:
+ pam_unix.so
+ pam_ldap.so
+ pam_mkhomedir.so
+ pam_mount.so

# Tipus de control

# Bàsics

Cal tenir en compte l'ordre d'aquestes.

## sufficient

Si ha anat bé plega i retorna que ha anat bé. Si falla l'ignora i no afecta a la resta de l'stack (Stack, conjunt de regles del mateix tipus)

Si és l'únic sufficient i falla, dóna error en comptes d'ignorar-se.
`auth       sufficient   pam_unix.so`

## required

Ha d'anar bé si és vol èxit, si va bé continua executant, si falla retorna error però abans executa tot l'stack de regles del mateix grup.

## requisite

Com el required, però si falla plega.

## optional

Intenta aplicar l'objecte, sense importar el resultat.

El resultat importa NOMÉS si és l'únic mòdul del stack del grup.

## include

Inclou a continuació totes les linies del tipus especificat del fitxer especificat com a argument.

Inclou de *system-auth* totes les regles però només avalua les de tipus auth
`auth		include		system-auth`

Es com si fèssim un cortar i pegar.

Si hi ha un done o un die plega de realitzar tot l'stack, regles del fitxer i les regles incloses amb el include.

## Substack

Funciona igual que l'include, la diferència és:

Si hi ha un done o un die plega de realitzar les linies del substack, són només les regles incloses amb el substack

# Avançats

## Ignore

S'ignora.

## Bad

Ha anat malament

## Die

Ha anat malament i vol plegar

## Ok

Ha anat bé

## Done

Ha anat bé i vol plegar

## N

Numero de linies a saltar

## Reset

Torna a evaluar tot

Si es success plega:
```
sufficient
[​ success=done​ new_authtok_reqd=done default=ignore]
```
Si es success OK pero no plega i per defecte dona error:
```
required,
[success=ok new_authtok_reqd=ok ignore=ignore ​ default=bad​ ]
```
Si es success OK pero no plega pero per defecta falla i plega:
```
requisite
[success=ok new_authtok_reqd=ok ignore=ignore ​ default=die​ ]
```

# Exemples control avançats

Si no es success salta 1 i va a parar al *deny*, si es success ho ignora i aplica el *permit*.
```
account    sufficient   pam_permit.so
auth       [default=1 success=ignore] pam_succeed_if.so debug uid = 1001
auth       sufficient pam_permit.so
auth       sufficient pam_deny.so
```
Quan l'usuari no tingui el uid 1001 es podrà canviar el finger.
```
account    sufficient   pam_permit.so
auth       [success=2 default=ignore] pam_succeed_if.so debug uid = 1001
auth       optional     pam_echo.so [ usuari uid NO 1001 ]
auth       sufficient   pam_permit.so
auth       sufficient   pam_deny.so
```

# Biblioteca de Objectes Compartits

**man pam_xxx(8)**

pam_permit.so --> Sempre ho permet
pam_deny.so --> Sempre ho nega
pam_unix.so --> Ha de ser un usuari unix.
pam_succeed_if.so --> Èxit si s'asegura la condició:
pam_env.so --> Carrega les variables d'entorn
pam_limits.so --> Carrega els limits del sistema (Ex: Limit a la CPU d'un usuari)
pam_pwquality --> Qualitat del password, nº intents, caràcters mínims... (man pwquality.conf)
pam_time.so --> Temps del compte de l'usuari. (Només funciona a Account)
pam_mkhomedir.so --> Si l'usuari no té home creat al sistema el crea.

# authconfig

`dnf -y install authconfig`

Genera les configuracions automàticament a system-auth, no necessito afegir i modificar les regles

Exemple de configuració amb autenticació ldap al contenidor de hostpam19:
```
authconfig --enableshadow --enablelocauthorize \
--enableldap \
--enableldapauth \
--enablemkhomedir \
--ldapserver='ldapserver' \
--ldapbase='dc=edt,dc=org' \
--updateall
```

Es com si afegim les regles pam_ldap.so al system-auth, però intuitiu i més segur.
