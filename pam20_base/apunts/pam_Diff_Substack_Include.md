# Diferència entre Substack i Include

---------------------------------------------------------------------

# Include vs Substack

1. Explica la diferència de funcionament entre els controls include i substack.

`Include:`

Afegeix a les regles que s'evaluen tot el contingut de regles que pertany al fitxer de configuració especificat com a argument:
```
auth  include   fitxer
auth  optional  pam_echo.so [ Prova ]
```

Tot i que el fitxer contingui regles de diversos controls, només s'avaluen les del control actual, en aquest cas *auth*.

En el cas de que hi hagués una ordre amb un *done* o un *die*, deixa d'aplicar les regles, tot el conjunt, les que hem afegit amb el include i les que hi havia a continuació d'aquest, és a dir, no aplicaria l'echo.

`Substack:`

Funciona igual que l'include, la diferència és:

Si hi ha un *done* o un *die* plega de realitzar les linies del substack, són només les regles incloses amb el substack

2. Escriu una configuració que serveixi per mostrar la diferència entre usar include o substack. Ha de contenir una línia de configuració que si la canviem per un o altre ha de mostrar clarament que actuen diferent:

File chfn
```
auth       optional     pam_echo.so [ Diferencia entre Include i Substack ]
auth       include      proves.txt
auth       sufficient   pam_echo.so [ Ho veu tothom i tambe local2 en cas de ser substack! ]
```
Subfitxer que es crida
```
auth       optional     pam_echo.so [ He arribat al fitxer extern ]
auth       [default=ignore success=done] pam_succeed_if.so debug user = local2
auth       optional     pam_echo.so [ Aixo ho veu tothom excepte local2 ]
```

# Salts condicionals
3. Escriu el fragment d’una configuració PAM de ​ session ​ on es validi que si l’usuari és el uid 1001 és success i si no eś fail.
```
auth    [ default=1 success=ignore ] pam_succeed_if.so debug uid = 1001
auth    sufficient pam_permit.so
auth    sufficient pam_deny.so
```

4. Escriu el fragment d’una configuració PAM de ​ session ​ on es validi que si l’usuari és el uid 1001 mostra un missatge i fa success i si no eś fail (sense missatge).
```
auth    [ default=2 success=ignore ] pam_succeed_if.so debug uid = 1001
auth    optional pam_echo.so [ Es l'usuari amb uid 1001 ]
auth    sufficient pam_permit.so
auth    sufficient pam_deny.so
```

5. Escriu el fragment d’una configuració PAM de ​ session ​ on es validi que si l’usuari és el uid 1001 fa success i si no mostra un missatge i fa fail.
```
auth    [ default=1 success=ignore ] pam_succeed_if.so debug uid = 1001
auth    sufficient pam_permit.so
auth    optional pam_echo.so [ No es l'usuari amb uid 1001 ]
auth    sufficient pam_deny.so
```

6. Escriu el fragment d’una configuració PAM de ​ session ​ on es validi que si l’usuari és el uid 1001 mostra un missatge i fa success i si no mostra un missatge d’error i fa fail.
```
auth    [ default=2 success=ignore ] pam_succeed_if.so debug uid = 1001
auth    optional pam_echo.so [ Es l'usuari amb uid 1001 ]
auth    sufficient pam_permit.so
auth    optional pam_echo.so [ No es l'usuari amb uid 1001 ]
auth    sufficient pam_deny.so
```
