# Pràctica amb pam_mount.so i pam_mkhomedir.so

---------------------------------------------------------------------

Primer comprovarem que tenim un contenidor ldapserver actiu i funcional.

Necessitarem que el contenidor ldap i el contenidor on realitzarem els muntatges estiguin a la mateixa xarxa, de manera que puguin comunicar-se.

En el meu cas el contenidor ldapserver està a 172.20.0.2 i la xarxa a la que pertany és *ldapnet*.

Comandes:
```
$ docker network list
$ docker inspect ldapserver
$ ldapsearch -x -LLL -h 172.20.0.2 -b 'dc=edt,dc=org'
```

# Directoris NFS

Al nostre host editem el fitxer /etc/exports per a indicar quins directoris volem compartir per xarxa.

Compartirem aquests
```
/usr/share/man *(rw,sync)
/usr/share/doc *(rw,sync)
```

Aleshores arranquem el servei NFS i comprovem que ho estem exportant:
```
# systemctl start nfs-server
# exportfs -a
# exportfs -s
/usr/share/man *(rw,sync,....)
/usr/share/doc  *(rw,sync,...)
```
# Instal·lació

Arranquem una màquina Fedora 27, on hi instal·larem tot.

`docker run --rm --name pam -h pam --net ldapnet --privileged -it fedora:27 /bin/bash`

Instal·lem els programes:
`dnf -y install vim procps util-linux-user passwd pam_mount nfs-utils`

# Configuració de fitxers.

Utilitzarem el mòdul pam_mount (man pam_mount)

## /etc/security/pam_mount.conf

Afegim les entrades per cada directori NFS que volguem muntar al sistema.

Podem treure els exemples del man del fitxer (man pam_mount.conf) a l'apartat de NFS:
```
NFS mounts
    <volume fstype="nfs" server="fileserver" path="/home/%(USER)"  mount‐
    point="~" />
```
Afegim l'entrada modificant els valors de l'exemple.
Hem de tenir en compte la direcció IP del servidor, en el nostre cas serà el gateway de Docker per a que pugui rebre els directoris compartits del nostre host. Queda així:
```
<volume fstype="nfs" server="172.20.0.1" path="/usr/share/man"  mountpoint="~/manuals/" />

<volume fstype="nfs" server="172.20.0.1" path="/usr/share/doc"  mountpoint="~/documentacio/" />
```
Comprovarem també la línea `<mkmountpoint enable="1" remove="true" />`, aquesta crea el directori en cas de que no estigui creat, i al finalitzar la connexió elimina el directori.

## /etc/pam.d/system-auth

Afegim les regles per a muntar de pam_mount i pam_mkhomedir, per a configurar els fitxers, s'apliquen a *auth* i *session*.

`man pam_mount`
`man pam_mkhomedir`

#### Suggerència

pam_mount no té use_first_pass, per tant si el posem davant de pam_unix amb el first_pass només demanarà una contrasenya (system-auth).

```
#%PAM-1.0
# This file is auto-generated.
# User changes will be destroyed the next time authconfig is run.
auth        required      pam_env.so
auth        optional      pam_mount.so
auth        sufficient    pam_unix.so try_first_pass nullok
auth        required      pam_deny.so

account     required      pam_unix.so

password    requisite     pam_pwquality.so try_first_pass local_users_only retry=3 authtok_type=
password    sufficient    pam_unix.so try_first_pass use_authtok nullok sha512 shadow
password    required      pam_deny.so

session     optional      pam_keyinit.so revoke
session     required      pam_limits.so
-session     optional      pam_systemd.so
session     [success=1 default=ignore] pam_succeed_if.so service in crond quiet use_uid
session     required      pam_unix.so
session     required      pam_mkhomedir.so
session     optional      pam_mount.so
```

# Comprovem:

```
[root@pam /]# su - local1
Creating directory '/home/local1'.
reenter password for pam_mount:
[local1@pam ~]$ ll
total 44
drwxr-xr-x. 1050 root root 36864 Nov 19 11:13 documentacio
drwxr-xr-x.   52 root root  4096 Sep 26 09:59 manuals
[local1@pam ~]$ tree /home/ -L 2
/home/
`-- local1
    |-- documentacio
    `-- manuals
```

Comprovem que passa quan sortim de la sessió:
```
[local1@pam ~]$ logout
[root@pam /]# tree /home/
/home/
`-- local1
```
