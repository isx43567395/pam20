# Implantació del servei nss-pam-ldap

[Documentació](https://arthurdejong.org/nss-pam-ldapd/setup)

Comprovem que tenim un servidor LDAP actiu i funcional amb dades.
```
docker run --rm --name ldapserver --net ldapnet -h ldapserver -d isx46420653/ldapserver
```

Arranquem un contenidor:
```
docker run --rm --name pam --net ldapnet -h pam --privileged -it isx46420653/hostpam19:auth /bin/bash
```

Instal·lem: sudo dnf -y install nss-pam-ldapd

Comprovem que hem instal·lat: `rpm -ql nss-pam-ldapd`

Editem /etc/nslcd.conf: (Xarxa privada, permet identificar per nom de contenidor)
```
uri ldap://ldapserver/
base dc=edt,dc=org
```

No editem però observem el contingut de /etc/nscd.conf.

Editem /etc/nsswitch.conf: (sss --> Kerberos)
```
# Example:
#passwd:    db files nisplus nis
#shadow:    db files nisplus nis
#group:     db files nisplus nis

passwd:     files ldap systemd
shadow:     files ldap
group:      files ldap systemd

#hosts:     db files nisplus nis dns
hosts:      files dns myhostname
```
passwd (Usuaris), files (Locals), ldap (ldapserver)

Si utilitzem authconfig ho fa automàticament amb *--enableldap*

Iniciem: /sbin/nslcd && /sbin/nscd

Comprovem:
```
$ getent passwd Pau
Pau:x:1000:1000:Pau:/home/Pau:/bin/bash

$ getent passwd jordi
jordi:*:5004:100:Jordi Mas:/tmp/home/jordi:
```

Com a root ens podem autenticar com a usuaris ldap:
`[root@pam docker]# su - pere`

Cal permetre l'autenticació a tots els usuaris.

Modifiquem /etc/pam.d/system-auth

Comprovem:
```
[root@pam pam.d]# su - pere
[pere@pam ~]$ id
uid=5001(pere) gid=100(users) groups=100(users)
[pere@pam ~]$ su - anna
pam_mount password:
id: cannot find name for group ID 600
```
No troba el grup però funciona correctament.

Canviem la contrasenya (Al tornar a autenticar ja ens funciona):
```
[user01@pam ~]$ passwd
Changing password for user user01.
(current) LDAP Password:
New password:
Retype new password:
passwd: all authentication tokens updated successfully.
```

Si posem la ordre de session de pam_mount abans dels sufficient al fer l'autenticació realitzarà els mount.

Comprovem al autenticar amb un usuari.

# Amazon
Ja tenim el servidor LDAP amb els grups ben formats i les dades coherents, el configurarem a la AMI de Amazon. Aleshores al host local, per a no fer modificacions als contenidors ja creats de hostpam19:auth, hem d'indicar-li on ha d'anar a buscar l'informació. Per això modifiquem el fitxer `/etc/hosts`:
```
IP_Servidor ldapserver    
```
